package qa.emp;


import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.junit5.JUnit5Mockery;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeLogicTest {


	@Test
	public void test_getAverageSalary() {
		Mockery ctx = new JUnit5Mockery();
		List<Employee> employees = Arrays.asList(
				new Employee(1, "Fred", "Bloggs", 47, 23500, "ADMIN"),
				new Employee(1, "John", "Smith", 42, 24500, "ADMIN")
			);
		EmployeeDAO dao = ctx.mock(EmployeeDAO.class);
	
		ctx.checking(new Expectations() {
			{
				oneOf(dao).login("root", "password");
				oneOf(dao).getAllEmployees();
				will(returnValue(employees));
			}
		});
		
		EmployeeLogic app = new EmployeeLogic(dao, "root", "password");

		assertEquals(24000.0, app.getAverageSalary(), 0.001);
	}

	@Test
	public void test_getHighestEarner() {
		Mockery ctx = new JUnit5Mockery();
		List<Employee> employees = Arrays.asList(
				new Employee(1, "Fred", "Bloggs", 47, 23500, "ADMIN"),
				new Employee(1, "John", "Smith", 42, 24500, "ADMIN")
		);
		EmployeeDAO dao = ctx.mock(EmployeeDAO.class);

		ctx.checking(new Expectations() {
			{
				oneOf(dao).login("root", "password");
				oneOf(dao).getAllEmployees();
				will(returnValue(employees));
			}
		});

		EmployeeLogic app = new EmployeeLogic(dao, "root", "password");

		assertEquals(employees.get(1), app.getHighestEarner()); // John
	}



}