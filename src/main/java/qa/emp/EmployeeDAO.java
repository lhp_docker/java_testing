package qa.emp;

import java.util.List;

public interface EmployeeDAO {

	public void login(String username, String password);

	public List<Employee> getAllEmployees();
	
	public List<Employee> getEmployeesByNameMatch(String nameFragment);
	
	public Employee getEmployeeById(int id);
	
	public int getNextFreeId();

	public boolean insertEmployee(Employee employee);
	
	public boolean deleteEmployee(Employee employee);

	public void logout();
}